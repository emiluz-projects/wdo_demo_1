class LoginPage {
  get headingText() {
    return $('#login-title');
  }
  get emailLabel() {
    return $('#form-login > div:nth-child(1) > label');
  }
  get emailInput() {
    return $('#loginEmail');
  }
  get passwordLabel() {
    return $('#form-login > div:nth-child(2) > label');
  }
  get passwordInput() {
    return $('#loginPassword');
  }
  get rememberLoginCheckbox() {
    return $('#rememberLoginChk');
  }
  get rememberLoginLabel() {
    return $('#form-login > div.form-check > label');
  }

  get submitBtn() {
    return $('#form-login > button');
  }

  get overlay() {
    return $('#overlay');
  }
}

module.exports = new LoginPage();
