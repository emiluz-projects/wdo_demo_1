const assert = require('assert');

const LoginPage = require('../pages/login.page');
const HeaderPage = require('../pages/header.page');

describe('Login test suite', () => {
  // valid email: 1@2.com
  // valid password: password
  it('should display error when password is missing', () => {
    browser.url('');

    LoginPage.emailInput.setValue('test@test.com');
    LoginPage.submitBtn.click();

    assert.strictEqual(browser.isAlertOpen(), true, 'Alert is not open');
    assert.strictEqual(browser.getAlertText(), 'Please enter an email and password', 'Alert text is not equal');
    browser.acceptAlert();
    assert.strictEqual(browser.isAlertOpen(), false, 'The alert is still open');
  });
  it('should display error when email is missing', () => {
    browser.url('');

    LoginPage.passwordInput.setValue('password');
    LoginPage.submitBtn.click();

    assert.strictEqual(browser.isAlertOpen(), true, 'Alert is not open');
    assert.strictEqual(browser.getAlertText(), 'Please enter an email and password', 'Alert text is not equal');
    browser.acceptAlert();
    assert.strictEqual(browser.isAlertOpen(), false, 'The alert is still open');
  });
  it('should display error when password and email are missing', () => {
    browser.url('');

    LoginPage.submitBtn.click();

    assert.strictEqual(browser.isAlertOpen(), true, 'Alert is not open');
    assert.strictEqual(browser.getAlertText(), 'Please enter an email and password');

    browser.acceptAlert();
    assert.strictEqual(browser.isAlertOpen(), false, 'The alert is still open');
  });
  it('should display error when email is incorrect', () => {
    browser.url('');

    LoginPage.emailInput.setValue('1@gs2.com');
    LoginPage.passwordInput.setValue('password');
    LoginPage.submitBtn.click();

    assert.strictEqual(browser.isAlertOpen(), true, 'Alert is not open');
    assert.strictEqual(browser.getAlertText(), 'Invalid email and password');

    browser.acceptAlert();
    assert.strictEqual(browser.isAlertOpen(), false, 'The alert is still open');
  });
  it('should display error when password is incorrect', () => {
    browser.url('');

    LoginPage.emailInput.setValue('1@2.com');
    LoginPage.passwordInput.setValue('test@testing.com');
    LoginPage.submitBtn.click();

    assert.strictEqual(browser.isAlertOpen(), true, 'Alert is not open');
    assert.strictEqual(browser.getAlertText(), 'Invalid email and password');

    browser.acceptAlert();
    assert.strictEqual(browser.isAlertOpen(), false, 'The alert is still open');
  });
  it('should display error when password case is incorrect', () => {
    browser.url('');

    LoginPage.emailInput.setValue('1@2.com');
    LoginPage.passwordInput.setValue('PASSWORD');
    LoginPage.submitBtn.click();

    assert.strictEqual(browser.isAlertOpen(), true, 'Alert is not open');
    assert.strictEqual(browser.getAlertText(), 'Invalid email and password');

    browser.acceptAlert();
    assert.strictEqual(browser.isAlertOpen(), false, 'The alert is still open');
  });

  it('should login with valid email and password', () => {
    browser.url('');

    LoginPage.emailInput.setValue('1@2.com');
    LoginPage.passwordInput.setValue('password');
    LoginPage.submitBtn.click();

    assert.strictEqual(LoginPage.overlay.isDisplayed(), false);
  });

  it('should remember login credentials', () => {
    browser.url('');
    LoginPage.emailInput.setValue('1@2.com');
    LoginPage.passwordInput.setValue('password');
    LoginPage.rememberLoginCheckbox.click();
    LoginPage.submitBtn.click();
    assert.strictEqual(LoginPage.overlay.isDisplayed(), false);

    HeaderPage.logoutLink.click();
    assert.strictEqual(LoginPage.overlay.isDisplayed(), true);
    assert.strictEqual(LoginPage.emailInput.getValue(), '1@2.com');
    assert.strictEqual(LoginPage.passwordInput.getValue(), 'password');
    assert.strictEqual(LoginPage.rememberLoginCheckbox.isSelected(), true);
  });
});
